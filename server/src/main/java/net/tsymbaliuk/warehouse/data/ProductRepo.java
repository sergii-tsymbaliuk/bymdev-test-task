package net.tsymbaliuk.warehouse.data;

import net.tsymbaliuk.warehouse.pojos.db.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepo extends CrudRepository<Product, Long> {
    List<Product> findBySku(String sku);
    List<Product> findByName(String name);
}
