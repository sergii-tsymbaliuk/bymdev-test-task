package net.tsymbaliuk.warehouse.data;

import net.tsymbaliuk.warehouse.pojos.db.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CategoryRepo extends CrudRepository<Category, Long> {
    List<Category> findByName(String categoryName);
}
