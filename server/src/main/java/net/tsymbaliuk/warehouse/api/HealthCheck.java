package net.tsymbaliuk.warehouse.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicBoolean;

@RestController
@RequestMapping(path="/status")
public class HealthCheck {

    @Autowired()
    @Qualifier("serviceReadyFlag")
    private AtomicBoolean serviceReadyFlag;


    @GetMapping(path="/", produces = "text/plain;charset=UTF-8")
    public ResponseEntity<?> checkServiceReady() {
        ResponseEntity<?> resp;
        if (serviceReadyFlag.get()) {
            resp = new ResponseEntity<>("yes", HttpStatus.OK);
        } else {
            resp = new ResponseEntity<>("No", HttpStatus.SERVICE_UNAVAILABLE);
        }
        return resp;
    }
}
