package net.tsymbaliuk.warehouse;

import net.tsymbaliuk.warehouse.data.CategoryRepo;
import net.tsymbaliuk.warehouse.data.ProductRepo;
import net.tsymbaliuk.warehouse.pojos.api.RequestOrderItem;
import net.tsymbaliuk.warehouse.pojos.db.Category;
import net.tsymbaliuk.warehouse.pojos.db.Order;
import net.tsymbaliuk.warehouse.pojos.db.Product;
import net.tsymbaliuk.warehouse.service.HibernateIndexingService;
import net.tsymbaliuk.warehouse.service.HibernateSearchService;
import net.tsymbaliuk.warehouse.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@SpringBootApplication(exclude = {ErrorMvcAutoConfiguration.class})
public class Application {
    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    @Scope("singleton")
    public AtomicBoolean serviceReadyFlag() {
        return new AtomicBoolean(false);
    }

    @Bean
    public CommandLineRunner demo(CategoryRepo categoryRepo, ProductRepo productRepo, OrderService orderService,
                                  HibernateIndexingService indexingService,
                                  @Qualifier("serviceReadyFlag") AtomicBoolean serviceReadyFlag) {
        return (args) -> {
            indexingService.initiateIndexing();
            // Create & few categories
            Category groceries = categoryRepo.save(new Category("Groceries"));
            Category electronics = categoryRepo.save(new Category("Electronics"));
            Category homeAndGarden = categoryRepo.save(new Category("Home&Garden"));

            // save a few Products
            productRepo.save(new Product("PepperJack", 10.0, "GR-PJ-01", groceries));
            productRepo.save(new Product("Philadelphia", 5.0, "GR-PH-02", groceries));
            productRepo.save(new Product("WhitePotato", 1.0, "GR-WP-03", groceries));
            productRepo.save(new Product("Cheddar", 10.0, "GR-CH-04", groceries));
            productRepo.save(new Product("Olive Oil", 10.0, "GR-OO-05", groceries));

            productRepo.save(new Product("iPhone", 800.0, "EL-IP-01", electronics));
            productRepo.save(new Product("Samsung", 750.0, "EL-SS-02", electronics));

            productRepo.save(new Product("Screw Driver", 75.0, "HG-WP-03", homeAndGarden));
            productRepo.save(new Product("Chain Saw", 200.0, "HG-CS-04", homeAndGarden));
            productRepo.save(new Product("Nails", 7.0, "HG-Nl-05", homeAndGarden));

            // fetch all categories
            log.info("Categories:");
            log.info("-------------------------------");
            for (final Category category : categoryRepo.findAll()) {
                log.info(category.toString());
            }
            log.info("");

            // fetch all products
            log.info("Products:");
            log.info("-------------------------------");
            for (final Product product : productRepo.findAll()) {
                log.info(product.toString());
            }
            log.info("");

            // save few orders
            Instant timeStamp = Instant.now().minus(2, ChronoUnit.DAYS);
            orderService.postOrder(
                    List.of(
                            new RequestOrderItem(4L, 1.0),
                            new RequestOrderItem(5L, 2.0),
                            new RequestOrderItem(10L, 6.0),
                            new RequestOrderItem(13L, 9.0)),
                    timeStamp.toEpochMilli());

            orderService.postOrder(
                    List.of(
                            new RequestOrderItem(7L, 3.0),
                            new RequestOrderItem(8L, 4.0),
                            new RequestOrderItem(9L, 5.0),
                            new RequestOrderItem(11L, 7.0)),
                    timeStamp.toEpochMilli());

            orderService.postOrder(List.of(
                    new RequestOrderItem(12L, 8.0)));

            orderService.postOrder(List.of(
                    new RequestOrderItem(5L, 1.0),
                    new RequestOrderItem(6L, 2.0),
                    new RequestOrderItem(7L, 3.0),
                    new RequestOrderItem(8L, 4.0),
                    new RequestOrderItem(9L, 5.0),
                    new RequestOrderItem(10L, 6.0),
                    new RequestOrderItem(11L, 7.0),
                    new RequestOrderItem(12L, 8.0),
                    new RequestOrderItem(13L, 9.0)));

            // fetch all orders
            log.info("Orders:");
            log.info("-------------------------------");
            for (final Order order : orderService.getOrders()) {
                log.info(order.toString());
            }
            log.info("");
            serviceReadyFlag.getAndSet(true);
        };
    }
}
