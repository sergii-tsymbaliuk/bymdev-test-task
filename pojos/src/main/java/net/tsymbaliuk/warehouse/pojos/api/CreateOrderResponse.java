package net.tsymbaliuk.warehouse.pojos.api;

public class CreateOrderResponse {
    private Long orderNumber;

    public CreateOrderResponse() {
    }

    public CreateOrderResponse(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Long getOrderNumber() {
        return orderNumber;
    }
}
