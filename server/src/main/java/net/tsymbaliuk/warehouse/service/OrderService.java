package net.tsymbaliuk.warehouse.service;

import net.tsymbaliuk.warehouse.pojos.db.Order;
import net.tsymbaliuk.warehouse.pojos.api.RequestOrderItem;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

@Service
public interface OrderService {
    Iterable<Order> getOrders();
    Order getOrder(Long orderId);
    Order postOrder(List<RequestOrderItem> requestOrderItems) throws ProductNotFoundException;
    Order postOrder(List<RequestOrderItem> requestOrderItems, Long timeStamp) throws ProductNotFoundException;
}
