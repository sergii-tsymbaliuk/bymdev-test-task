package net.tsymbaliuk.warehouse.service;

import net.tsymbaliuk.warehouse.data.OrderItemsRepo;
import net.tsymbaliuk.warehouse.data.OrdersRepo;
import net.tsymbaliuk.warehouse.data.ProductRepo;
import net.tsymbaliuk.warehouse.pojos.api.RequestOrderItem;
import net.tsymbaliuk.warehouse.pojos.db.Category;
import net.tsymbaliuk.warehouse.pojos.db.Order;
import net.tsymbaliuk.warehouse.pojos.db.OrderItem;
import net.tsymbaliuk.warehouse.pojos.db.Product;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.AdditionalMatchers;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OrderServiceImplTest {

    @Mock
    private ProductRepo productsRepo;
    @Mock
    private OrdersRepo ordersRepo;
    @Mock
    private OrderItemsRepo orderItemsRepo;

    @Captor
    private ArgumentCaptor<OrderItem> orderItemCaptor;

    private OrderServiceImpl orderService;

    @BeforeEach
    public void setUp() {
        orderService = new OrderServiceImpl(productsRepo, ordersRepo, orderItemsRepo);
    }


    @Test
    void postOrder() throws ProductNotFoundException {
        List<RequestOrderItem> requestOrderItems = List.of(
                new RequestOrderItem(1L, 10.0),
                new RequestOrderItem(2L, 20.0),
                new RequestOrderItem(3L, 30.0));

        Category category1 = new Category("cat1");
        category1.setId(10L);
        Category category2 = new Category("cat2");
        category2.setId(20L);

        Product p1 = new Product("Prod1", 10.0, "SKU1",   category1);
        p1.setId(1L);
        Product p2 = new Product("Prod2", 20.0, "SKU2",   category1);
        p2.setId(2L);
        Product p3 = new Product("Prod3", 30.0, "SKU3",   category2);
        p3.setId(3L);

        List<Product> products = List.of(p1, p2, p3);
        when(productsRepo.findById(eq(1l))).thenReturn(Optional.of(p1));
        when(productsRepo.findById(eq(2l))).thenReturn(Optional.of(p2));
        when(productsRepo.findById(eq(3l))).thenReturn(Optional.of(p3));

        Order order = new Order();
        order.setId(4L);
        when(ordersRepo.save(AdditionalMatchers.not(eq(order)))).thenReturn(order);

        Order actualOrder = orderService.postOrder(requestOrderItems);

        verify(orderItemsRepo, times(3)).save(orderItemCaptor.capture());
        List<OrderItem> actualOrderItems = orderItemCaptor.getAllValues();

        assertThat(actualOrderItems, IsCollectionWithSize.hasSize(3));
        assertThat(actualOrderItems.stream().map(oi ->oi.getProduct().getId()).collect(Collectors.toList()),
                containsInAnyOrder(1L, 2L, 3L));
        assertThat(actualOrderItems.stream().map(oi ->oi.getOrder().getId()).collect(Collectors.toList()),
                containsInAnyOrder(4L, 4L, 4L));
        assertThat(actualOrderItems.stream().map(oi ->oi.getQuantity()).collect(Collectors.toList()),
                containsInAnyOrder(10.0, 20.0, 30.0));
    }
}