package net.tsymbaliuk.warehouse.service;

import net.tsymbaliuk.warehouse.data.OrderItemsRepo;
import net.tsymbaliuk.warehouse.data.OrdersRepo;
import net.tsymbaliuk.warehouse.data.ProductRepo;
import net.tsymbaliuk.warehouse.pojos.db.Order;
import net.tsymbaliuk.warehouse.pojos.api.RequestOrderItem;
import net.tsymbaliuk.warehouse.pojos.db.OrderItem;
import net.tsymbaliuk.warehouse.pojos.db.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;

import static org.springframework.transaction.annotation.Isolation.REPEATABLE_READ;

@Service
public class OrderServiceImpl implements OrderService{
    private static final Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);

    private ProductRepo productsRepo;
    private OrdersRepo ordersRepo;
    private OrderItemsRepo orderItemsRepo;

    @Autowired
    public OrderServiceImpl(ProductRepo productsRepo, OrdersRepo ordersRepo, OrderItemsRepo orderItemsRepo) {
        this.productsRepo = productsRepo;
        this.ordersRepo = ordersRepo;
        this.orderItemsRepo = orderItemsRepo;
    }

    @Override
    @Transactional(isolation = REPEATABLE_READ, rollbackFor=Exception.class)
    public Order postOrder(List<RequestOrderItem> requestOrderItems) throws ProductNotFoundException {
        return postOrder(requestOrderItems, Instant.now().toEpochMilli());
    }

    @Override
    @Transactional(isolation = REPEATABLE_READ, rollbackFor=Exception.class)
    public Order postOrder(List<RequestOrderItem> requestOrderItems, Long timeStamp)
            throws ProductNotFoundException {
        log.info("Receive order for {}", Arrays.toString(requestOrderItems.toArray()));

        Order order = new Order();
        order.setTimeStamp(timeStamp);
        order = ordersRepo.save(order);

        for (RequestOrderItem requestOrderItem : requestOrderItems) {
            Long productId = requestOrderItem.getProductId();
            Optional<Product> product = productsRepo.findById(productId);
            if (product.isEmpty()) {
                throw new ProductNotFoundException("Cannot find product " + productId);
            }
            orderItemsRepo.save(
                    new OrderItem(order, product.get(), requestOrderItem.getQuantity())
            );
        }
        log.info("Created order {}", order);
        return order;
    }

    @Override
    public Iterable<Order> getOrders() {
        return ordersRepo.findAll() ;
    }

    @Override
    public Order getOrder(Long orderId) {
        return ordersRepo.findById(orderId).orElse(null);
    }
}
